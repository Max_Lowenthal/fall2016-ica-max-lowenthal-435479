<?php
	require "db.php";
	
	if (is_null($_POST['name']) || is_null($_POST['email']) || is_null($_POST['age']) || is_null ($_POST['description'])|| is_null($_FILES['photo'])) {
		echo "Error: One or more posted variables were null.";
		exit;
	}
	
#http://www.w3schools.com/php/php_file_upload.asp
	$target_dir = "/uploads/";
	$target_file = $target_dir . basename($_FILES["photo"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["photo"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
		}
	}
	if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["photo"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

$name = $_POST['name'];
$email = $_POST['email'];
$age = (int) $_POST['age'];
$description = $_POST['description'];

$stmt = $mysqli->prepare("insert into matcher (name, email, pictureUrl, description, age) values (?, ?, ?, ?, ?)");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('ssssi', $name, $email, $target_file, $description,$age);
if (!$stmt->execute()) {
    echo "Insertion Failed";
    exit;
}
header('Location:show-users.php');




?>