CREATE TABLE matcher (
	
    id INT UNSIGNED AUTO_INCREMENT NOT NULL,
    name VARCHAR(50) NOT NULL,
	email varchar(100)
	pictureURL varchar(200)
	description tinytext
	age INT UNSIGNED NOT NULL
    PRIMARY KEY (id)
) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;