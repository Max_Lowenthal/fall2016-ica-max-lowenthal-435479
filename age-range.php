<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Matchmaking Site - Users by Age</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">

<h1>Users in Age Range</h1>

<?php

require "db.php";

$low = htmlspecialchars($_GET['low']);
$high = htmlspecialchars($_GET['high']);

$stmt = $mysqli->prepare("select name, email,pictureUrl,description, age from matcher WHERE age BETWEEN ? and ?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('ss', $low,$high);
$stmt->execute();
$stmt->bind_result($name, $email,$pictureUrl, $description, $age);
while ($stmt->fetch()) {
	echo "<ul>";
	echo "<li>".htmlentities($name)."</li>"."<br>";
	echo "<li>".htmlentities($email)."</li>"."<br>";
	echo "<li><img src='htmlentities($pictureUrl)' width = '300px'></li><br>";
	echo "<li>".htmlentities($description)."</li>"."<br>";
	echo "<li>".htmlentities($age)."</li>"."<br>";
	echo "</ul>";
}
$stmt->close();


?>

<a href="show-users.php">See all Users</a>
</div></body>
</html>
